import React from "react";

export default function Post (props) {
    return (
        <div className="post">
            <div className="post-img-keeper">
                <img src={props.imageUrl} className="post-img" alt="Awesome picture" />
            </div>
            <div className="post-content">
                <div className="location-line">
                    <div className="location-holder">
                        <i className="fa-solid fa-location-dot location-icon"></i>
                        <span className="country">{props.location}</span>
                        <a className="location-link" href={props.googleMapsUrl}>View on Google Maps</a>
                    </div>
                </div>
                <div className="place">
                    <h1>{props.title}</h1>
                </div>
                <div className="date-line">
                    <h3>{props.startDate} - {props.endDate}</h3>
                </div>
                <div className="post-text">
                    <span>{props.description}</span>
                </div>
            </div>
            <hr className="line-near-post" />
        </div>
    )
}


