import React from "react";
import Post from "./Post.jsx";
import data from "../data.jsx";

export default function Journal () {
    let posts = data.map(post => {
        return (
            <Post
                key={post.id}
                {...post}
            />
        )
    })

    return (
        <div className="journal">
            {posts}
        </div>
    )
}