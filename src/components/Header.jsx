import React from "react"

export default function Header () {
    return (
        <div className="header">
            <div className="header-content">
                <span>
                    <i className="fa-solid fa-globe globe-icon"></i>
                    my travel journal
                </span>
            </div>
        </div>
    )
}