import React from "react";
import Header from "./components/Header.jsx";
import Journal from "./components/Journal.jsx";
function App() {

  return (
      <div className="page">
          <Header />
          <Journal />
      </div>
  )
}

export default App
